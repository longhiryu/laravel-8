<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Dashboard\Http\Controllers\DashboardController;

Route::prefix('admin')->middleware(['auth:sanctum', 'verified', 'locale'])->group(function () {
    Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard.index');

    Route::get('/change-language/{lang}', [DashboardController::class,'language'])->name('change_language');
});
