<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'parent_id',
        'position',
        'is_active',
        'slug',
        'type'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Category\Database\factories\CategoryFactory::new();
    }

    public function tranlations()
    {
        return $this->hasOne(CategoryTranlation::class)->where('locale', config('app.locale'));
    }

    public function parent($id)
    {
        $data = CategoryTranlation::where('category_id', $id)->where('locale', config('app.locale'))->first();
        return $data;
    }
}
