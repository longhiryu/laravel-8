<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CategoryTranlation extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'locale',
        'name',
        'slug',
        'title',
        'long_description',
        'short_description'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Category\Database\factories\CategoryTranlationFactory::new();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
