<?php

use Illuminate\Http\Request;
use Modules\Category\Http\Controllers\Admin\Api\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('category')->group(function () {
    Route::get('/', [CategoryController::class,'index'])->name('api.category.index');
});

//Route::get('/category', [CategoryController::class,'index'])->name('api.category.index')->middleware('auth:api');
// Route::middleware('auth:api')->get('/category', function (Request $request) {
//     //Route::get('/', [CategoryController::class,'index'])->name('api.category.index');
//     return 'Route api category';
// });
