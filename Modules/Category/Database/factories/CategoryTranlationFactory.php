<?php
namespace Modules\Category\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Category\Entities\Category;

class CategoryTranlationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Category\Entities\CategoryTranlation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $locale = array(0 => 'en', 1 => 'vi');

        return [
            'category_id' => Category::inRandomOrder()->first()->id,
            'locale' => $locale[rand(0, 1)],
            'name' => $this->faker->name,
            'slug' => mySlugString($this->faker->name),
            'title'=> $this->faker->name,
            'long_description' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'short_description' => $this->faker->sentence($nbWords = 6, $variableNbWords = true)
        ];
    }
}
