<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\CategoryTranlation;

class CategoryTralationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        CategoryTranlation::factory(10)->create();
        //$this->call("CategoryTranlationTableSeeder");
    }
}
