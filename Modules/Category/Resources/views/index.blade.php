@extends('backend.layouts.main')

@section('content')
{{-- dd($data) --}}
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Categories</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
                        <li class="breadcrumb-item active">Categories</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title">Categories</h3>
                                <a class="btn btn-primary btn-sm" href="{{ route('categories.create') }}">{{ trans('phrases.add_new') }}</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="root"></div>
                            {{-- table-list --}}
                            <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Position</th>
                                    <th scope="col">Parent</th>
                                    <th scope="col">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($data as $category)
                                {{-- dd($category) --}}
                                  <tr>
                                    <th scope="row">{{ $category->id }}</th>
                                    <td>
                                        @if ($category->tranlations != null)
                                            {{ $category->tranlations->name }}
                                        @endif
                                    </td>
                                    <td>{{ $category->position }}</td>
                                    <td>
                                        @if($category->parent_id != null)
                                            {{ $category->parent($category->parent_id)->name }}   
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-success btn-sm" href="{{ route('categories.edit',$category->id) }}">
                                            <i class="fas fa-edit fa-sm"></i> Edit
                                        </a>
                                    </td>
                                  </tr>
                                @endforeach
                                </tbody> 
                              </table>
                            {{-- table-list --}}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
@endsection