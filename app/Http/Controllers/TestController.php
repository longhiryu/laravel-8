<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Modules\Category\Entities\Category;

class TestController extends Controller
{
    public function testSlug()
    {
        return mySlugString('Trịnh Hoàng Long');
    }

    public function testRelation()
    {
        $data = Category::find(8)->category_tranlations()->get();
        return response()->json($data, 200);
    }

    public function langVi()
    {
        App::setLocale('vi');
        return trans('phrases.hello');
    }

    public function langEn()
    {
        App::setLocale('en');
        return trans('phrases.hello');
    }
}
